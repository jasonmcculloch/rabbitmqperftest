echo off
cls

echo Starting a consumer...
start .\RabbitConsumer\bin\Debug\RabbitConsumer.exe true perf_test_durable localhost

pause

echo Starting a publisher...
start .\RabbitPublisher\bin\Debug\RabbitPublisher.exe true perf_test_durable localhost 200000

pause