﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace RabbitPublisher
{
    class Program
    {
        private const string _queueNameDurable = "perf_test_durable";
        private const string _queueNameNonDurable = "perf_test_non_durable";
        private const string _rabbitHost = "localhost";

        static void Main(string[] args)
        {
            var durable = true;
            var queueName = durable ? _queueNameDurable : _queueNameNonDurable;
            var rabbitHost = _rabbitHost;
            var totalMessages = 100;

            if (args.Length > 0)
            {
                durable = bool.Parse(args[0]);
            }
            if (args.Length > 1)
            {
                queueName = args[1];
            }
            if (args.Length > 2)
            {
                rabbitHost = args[2];
            }
            if (args.Length > 3)
            {
                totalMessages = int.Parse(args[3]);
            }

            var watch = new Stopwatch();
            watch.Start();

            var factory = new ConnectionFactory() { HostName = rabbitHost };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queueName,
                                     durable: durable,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var body = Encoding.UTF8.GetBytes(File.ReadAllText(path + "\\message.txt"));
                Console.WriteLine("Message size: {0} bytes", body.Length);

                Console.WriteLine("Publishing messages...");
                for (int count = 1; count <= totalMessages; count++)
                {
                    channel.BasicPublish(exchange: "",
                        routingKey: queueName,
                        basicProperties: null,
                        body: body);
                }
            }

            watch.Stop();
            Console.WriteLine("Took {0} seconds to publish {1} messages to the {2} queue '{3}' - {4}/sec",
                (watch.ElapsedMilliseconds / 1000),
                totalMessages,
                durable ? "Durable" : "Non-durable",
                queueName,
                (totalMessages / (watch.ElapsedMilliseconds / 1000.0)));

            Console.WriteLine("Press <Enter> to exit...");
            Console.ReadLine();
        }
    }
}
