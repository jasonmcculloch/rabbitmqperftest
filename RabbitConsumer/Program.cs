﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitConsumer
{
    class Program
    {
        private const string _queueNameDurable = "perf_test_durable";
        private const string _queueNameNonDurable = "perf_test_non_durable";
        private const string _rabbitHost = "localhost";

        static void Main(string[] args)
        {
            var durable = true;
            var queueName = durable ? _queueNameDurable : _queueNameNonDurable;
            var rabbitHost = _rabbitHost;
            if (args.Length > 0)
            {
                durable = bool.Parse(args[0]);
            }
            if (args.Length > 1)
            {
                queueName = args[1];
            }
            if (args.Length > 2)
            {
                rabbitHost = args[2];
            }

            var factory = new ConnectionFactory() { HostName = _rabbitHost };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queueName,
                                     durable: durable,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                channel.BasicQos(prefetchSize: 0,
                    prefetchCount: 1,
                    global: false);

                Console.WriteLine("Waiting for messages on queue {0}...", queueName);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);

                    channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                    Console.Write(".");
                };

                var result = channel.BasicConsume(queue: queueName,
                                        noAck: false,
                                        consumer: consumer);

                Console.WriteLine("Press <Enter> to exit...");
                Console.ReadLine();
            }
        }
    }
}
