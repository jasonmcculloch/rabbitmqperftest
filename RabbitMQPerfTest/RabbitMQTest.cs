﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using NUnit.Framework;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQPerfTest.Properties;

namespace RabbitMQPerfTest
{
    [TestFixture]
    public class RabbitMQTest
    {
        private const string _queueNameDurable = "perf_test_durable";
        private const string _queueNameNonDurable = "perf_test_non_durable";
        private const string _rabbitHost = "localhost";

        [Test]
        [TestCase(false, 100000)]
        [TestCase(true, 100000)]
        public void TestRabbitBasicPublishPerformance(bool durable, int totalMessages)
        {
            var queueName = durable ? _queueNameDurable : _queueNameNonDurable;
            var watch = new Stopwatch();
            watch.Start();

            var factory = new ConnectionFactory() { HostName =_rabbitHost };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queueName,
                                     durable: durable,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                //var body = Encoding.UTF8.GetBytes(string.Format("Test run starting {0}", DateTime.Now.ToLongTimeString()));
                var body = Encoding.UTF8.GetBytes(Resources.SampleMessage);

                for (int count = 1; count <= totalMessages; count++)
                {
                    channel.BasicPublish(exchange: "",
                        routingKey: queueName,
                        basicProperties: null,
                        body: body);
                }
            }

            watch.Stop();
            Console.WriteLine("Took {0} seconds to publish {1} messages to the {2} queue '{3}' - {4}/sec", 
                (watch.ElapsedMilliseconds/1000), 
                totalMessages, 
                durable ? "Durable" : "Non-durable", 
                queueName,
                (totalMessages / (watch.ElapsedMilliseconds / 1000)));
            Assert.IsTrue(true);
        }

        [Test]
        [TestCase(false, 2)]
        [TestCase(true, 2)]
        public void TestBasicConsumptionPerformance(bool durable, int waitSecsBetweenMsgCounts)
        {
            var queueName = durable ? _queueNameDurable : _queueNameNonDurable;
            var watch = new Stopwatch();
            watch.Start();
            var totalMessages = 0;

            var factory = new ConnectionFactory() { HostName = _rabbitHost };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {

                channel.BasicQos(prefetchSize: 0, 
                    prefetchCount: 1, 
                    global: false);

                Console.WriteLine("Waiting for messages...");

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);

                    channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                    totalMessages++;
                };

                var result = channel.BasicConsume(queue: queueName,
                                        noAck: false,
                                        consumer: consumer);

                while (channel.MessageCount(queueName) > 0)
                {
                    Thread.Sleep(waitSecsBetweenMsgCounts * 1000);
                }
            }

            watch.Stop();
            Console.WriteLine("Took {0} seconds to consume {1} messages from the {2} queue '{3}' - {4}/sec",
                (watch.ElapsedMilliseconds / 1000),
                totalMessages,
                durable ? "Durable" : "Non-durable",
                queueName,
                (totalMessages / (watch.ElapsedMilliseconds / 1000)));
            Assert.IsTrue(true);

        }
    }
}
